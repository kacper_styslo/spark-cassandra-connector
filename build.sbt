name := "spark_connector"

version := "1.0"

scalaVersion := "2.12.10"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % "3.1.2",
  "org.apache.spark" %% "spark-core" % "3.1.2",
  "org.apache.spark" %% "spark-streaming" % "3.1.2",
  "com.datastax.spark" %% "spark-cassandra-connector" % "3.1.0",
  "org.scalatest" %% "scalatest" % "3.0.8")
