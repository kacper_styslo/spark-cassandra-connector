import scala.language.postfixOps
import com.datastax.spark.connector._
import org.apache.spark.sql.SparkSession
import com.datastax.spark.connector.rdd.CassandraTableScanRDD
import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._
import scala.io.Source

object Connector {
  def main(args: Array[String]): Unit = {
    """
      Calling all functions from here.
      |""".stripMargin
    println("*" * 10 + " START CONNECTION! " + "*" * 10)
    val (spark_session, programmers_tb, devops_tb) = cassandra_config()

    println('\n' + "*" * 10 + " START DATABASE OPERATIONS " + "*" * 10 + '\n')
    check_cassandra(spark_session)
//    show_programmers_table(programmers_tb)
//    select_examples(programmers_tb)
//    where_examples(programmers_tb)
//    getting_seq(programmers_tb)
//    join_examples(programmers_tb, devops_tb)
//    flat_map_example(programmers_tb, devops_tb)
    count_words_from_file(spark_session)
    println('\n' + "*" * 10 + " STOP DATABASE OPERATIONS " + "*" * 10 + '\n')

    stop_cluster(spark_session)
    println("*" * 10 + " STOP CONNECTION! " + "*" * 10)
  }

  def cassandra_config(): (
    SparkSession,
      CassandraTableScanRDD[CassandraRow],
      CassandraTableScanRDD[CassandraRow]
    ) = {
    """
      Config for all operations on cassandra.
      |""".stripMargin
    val sc = SparkSession.builder
      .appName("connector_app")
      .master("local[2]")
      .config(
        "spark.sql.catalog.cassandra",
        "com.datastax.spark.connector.datasource.CassandraCatalog"
      )
      .config("spark.cassandra.connection.host", "localhost")
      .config(
        "spark.sql.extensions",
        "com.datastax.spark.connector.CassandraSparkExtensions"
      )
      .getOrCreate()
    sc.sparkContext.setLogLevel("WARN")

    val programmers_tb = sc.sparkContext.cassandraTable(
      keyspace = "projects",
      table = "programmers"
    ): CassandraTableScanRDD[CassandraRow]
    val devops_tb = sc.sparkContext.cassandraTable(
      keyspace = "projects",
      table = "devops"
    ): CassandraTableScanRDD[CassandraRow]

    (sc, programmers_tb, devops_tb)
  }

  def check_cassandra(spark_session: SparkSession): Unit = {
    """
      |Test if after connection we can show NAMESPACES & TABLES from cassandra.
      |""".stripMargin
    println('\n' + "*" * 10 + " check_cassandra START! " + "*" * 10)

    spark_session.sql("SHOW NAMESPACES FROM cassandra").show
    spark_session.sql("SHOW TABLES FROM cassandra.projects").show

    println('\n' + "*" * 10 + " check_cassandra STOP! " + "*" * 10 + '\n')
  }

  def select_examples(programmers_tb: CassandraTableScanRDD[CassandraRow]): Unit = {
    """
      |Some of simple selects examples.
      |""".stripMargin
    println('\n' + "*" * 10 + " select_examples START! " + "*" * 10 + '\n')

    programmers_tb.select("name").foreach(println)
    programmers_tb.select(columns = "name").rowReader

    println('\n' + "*" * 10 + " select_examples STOP! " + "*" * 10 + '\n')
  }

  def where_examples(programmers_tb: CassandraTableScanRDD[CassandraRow]): Unit = {
    """
      |Some of where single & multiple statements examples.
      |""".stripMargin
    println('\n' + "*" * 10 + " where_examples START! " + "*" * 10 + '\n')

    val single_statement = programmers_tb
      .filter(x =>
        x.getString("name")
          .contains("Kacper")
      )
      .take(1)
      .mkString(",")
    println(single_statement)

    val multiple_statement =
      programmers_tb
        .filter(x =>
          x.getString("name") == "Kacper"
            && x.getString("technology") == "Python"
        )
        .take(1)
        .mkString(",")
    println(multiple_statement)

    val multiple_statement_v2 = programmers_tb
      .filter(x =>
        x.getString("name") == "Kacper"
          && x.getString("technology") == "Python" || x.getString(
          "name"
        ) == "Piotr"
          && x.getString("technology") == "Scala"
      )
      .take(2)
      .mkString(",")
    println(multiple_statement_v2)

    val get_python_and_scala_programmers = programmers_tb
      .filter(x =>
        x.getString("technology") == "Python" || x.getString(
          "technology"
        ) == "Scala"
      )
      .take(4)
      .mkString(",")
    println(get_python_and_scala_programmers)

    println('\n' + "*" * 10 + " where_examples STOP! " + "*" * 10 + '\n')
  }

  def getting_seq(programmers_tb: CassandraTableScanRDD[CassandraRow]): Unit = {
    println('\n' + "*" * 10 + " getting_seq START! " + "*" * 10 + '\n')

    val seq_of_even_id = programmers_tb
      .filter(x => x.getInt("id") % 2 == 0)
      .take(4)
      .toSeq
    println(seq_of_even_id)

    val result = seq_of_even_id.flatMap { s =>
      Seq(s, s + " Mapped")
    }
    println(result)

    println('\n' + "*" * 10 + " getting_seq STOP! " + "*" * 10 + '\n')
  }

  def join_examples(
                     programmers_tb: CassandraTableScanRDD[CassandraRow],
                     devops_tb: CassandraTableScanRDD[CassandraRow]): Unit = {
    println('\n' + "*" * 10 + " join_examples START! " + "*" * 10 + '\n')

    val joined_tables = programmers_tb.joinWithCassandraTable(keyspaceName = "projects", tableName = "devops")
    joined_tables.collect.foreach(println) // union ? by default collect on id

    val joined_tables_v2 = programmers_tb.joinWithCassandraTable(keyspaceName = "projects", tableName = "devops")
    println(joined_tables_v2)
    val transformatio_with_joined_tables_v2 = programmers_tb.union(devops_tb)
    println(transformatio_with_joined_tables_v2)
    val continues_joined_tables_v2_transformation = transformatio_with_joined_tables_v2
      .map(x =>
        (x)
      )
      .take(12)
    println(continues_joined_tables_v2_transformation.mkString("\n"))

    println('\n' + "*" * 10 + " join_examples STOP! " + "*" * 10 + '\n')
  }

  def show_programmers_table(programmers_tb: CassandraTableScanRDD[CassandraRow]): Unit = {
    """
      |Test if after connection we can show rows.
      |""".stripMargin

    println("*" * 40)
    val pairs = programmers_tb.map(s => (s, 1)).reduceByKey(_ + _)
//    val counts = pairs.reduceByKey((a, b) => a + b)
    val print = pairs.collect.foreach(println)
//    print.foreach(println)
//    println("*" * 40)

    println("*" * 40)
    // val pairs2 = programmers_tb.flatMap(s => s: CassandraRow)
    val counts2 = pairs.reduceByKey((a, b) => a + b)
    val print2 = pairs.take(2)
    print2.foreach(println)
    println("*" * 40)

    val array1 = programmers_tb.collect() // only for testing propose
    array1.foreach(println)

    // RDD of CassandraRow
    val df3 = programmers_tb.filter(x => x.getString("name") == "Piotr")
    println(df3.count)
    println(df3.take(1).mkString(","))
  }

  def flat_map_example(programmers_tb: CassandraTableScanRDD[CassandraRow], devops_tb: CassandraTableScanRDD[CassandraRow]): Unit = {
    println("*" * 20, "flatMap example", "*" * 20)
//    val join_table = programmers_tb.joinWithCassandraTable(keyspaceName = "projects", tableName = "devops")
//    println(join_table)
//    println("*" * 20, "flatMap example", "*" * 20)
    val union_table = programmers_tb.union(devops_tb)
//    union_table.
    println(union_table)
    println(union_table.collect.foreach(println))
//    val word = union_table.flatMap(split(" "))


    println("*" * 20, "flatMap stop", "*" * 20)

  }

  def count_words_from_file(sparkSession: SparkSession): Unit = {
//    val fileName = sparkSession.sparkContext.textFile("file:///C:/Users/pbuga/IdeaProjects/spark-cassandra-connector/data/lokomotywa.txt")
    val fileName = sparkSession.sparkContext.textFile("file:///C:/Users/pbuga/IdeaProjects/spark-cassandra-connector/data/ThePhilosophersStone.txt")
    fileName.flatMap(line => line.split("\\W")).map(word => (word.toLowerCase, 1)).reduceByKey(_+_).sortBy(-_._2).take(20).foreach(println)
  }

  def stop_cluster(spark_session: SparkSession): Unit = {
    """
      |Always stop cluster after operations.
      |""".stripMargin
    spark_session.stop()
  }
}