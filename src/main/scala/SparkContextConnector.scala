import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.StreamingContext._


object SparkContextConnector  {
  def main(args: Array[String]): Unit = {
"""
  |Bellow commands to work in spark-submit shell
  |""".stripMargin
    val conf = new SparkConf().setMaster("local[2]").setAppName("NetworkWordCount")
    val ssc = new StreamingContext(conf, Seconds(5))
    val lines = ssc.socketTextStream("localhost", 9999)
//    val words = lines.flatMap(_.split(" "))
//    val pairs = words.map(word => (word, 1))
//    val wordCounts = pairs.reduceByKey(_ + _)
//    wordCounts.print()

    val focus_tweets_collection = lines.filter(text => text.toLowerCase.contains("happy") | text.toLowerCase.contains("money"))
    focus_tweets_collection.map(line => (line, 1)).reduceByKey(_ + _).print()
    ssc.start()
    ssc.awaitTermination()
  }
}



